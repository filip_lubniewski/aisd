import Utils.Car;
import Utils.List;

/**
 * Created by macbookpro on 07.03.2016.
 */
public class ArrayAutoOrderList implements List {
    private static final int DEFAULT_SIZE = 4;
    private int initialSize;
    private int size;
    private Object[] array;

    private int minAge;
    private int maxAge;

    public ArrayAutoOrderList(int initialSize) {
        this.initialSize = initialSize;
        clear();
    }

    public ArrayAutoOrderList() {
        this(DEFAULT_SIZE);
    }

    @Override
    public void add(Object value) {
        allocateNewSpaceIfNeeded(size + 1);

        Car carToAdd = (Car) value;

        if (size == 0) {
            minAge = maxAge = carToAdd.getAge();
            array[0] = carToAdd;
        } else {
            addCarToListByAge(carToAdd);
        }

        size++;
    }

    private void addCarToListByAge(Car carToAdd) {
        int ageOfTheCar = carToAdd.getAge();
        int indexIndicator = 0;

        if (ageOfTheCar <= minAge) {
            indexIndicator = 0;
            minAge = ageOfTheCar;
        } else if (ageOfTheCar >= maxAge) {
            indexIndicator = size;
            maxAge = ageOfTheCar;
        } else {
            while (((Car) array[indexIndicator]).getAge() < ageOfTheCar) {
                indexIndicator++;
            }
        }

        System.arraycopy(array, indexIndicator, array, indexIndicator + 1, size - indexIndicator);
        array[indexIndicator] = carToAdd;
    }

    private void allocateNewSpaceIfNeeded(int newSize) {
        if (array.length < newSize) {
            Object[] temporary = new Object[2 * size];
            System.arraycopy(array, 0, temporary, 0, size);
            array = temporary;
        }
    }

    @Override
    public void delete(Object value) {
        boolean found = false;

        for (int i = 0; i < size && !found; i++) {
            if (array[i].equals(value)) {
                System.arraycopy(array, i + 1, array, i, size - i + 1);
                found = true;
            }
        }

        size--;
        array[size] = null;

        if (size != 0) {
            minAge = ((Car) array[0]).getAge();
            maxAge = ((Car) array[size - 1]).getAge();
        }
    }

    @Override
    public Object get(int index) throws IndexOutOfBoundsException {
        checkBounds(index);
        return array[index];
    }

    private void checkBounds(int index) {
        if (index < 0 || index >= size()) throw new IndexOutOfBoundsException();
    }

    @Override
    public void set(int index, Object value) throws IndexOutOfBoundsException {
        checkBounds(index);

        boolean found = false;

        for (int i = 0; i < size && !found; i++) {
            if (array[i].equals(value)) {
                delete(array[i]);
                found = true;
            }
        }

        add(value);
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void clear() {
        array = new Object[initialSize];
        size = 0;

        minAge = 0;
        maxAge = 0;
    }
}
