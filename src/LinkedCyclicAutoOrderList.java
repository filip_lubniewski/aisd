import Utils.Car;
import Utils.List;

/**
 * Created by macbookpro on 06.03.2016.
 */
public class LinkedCyclicAutoOrderList implements List {

    private Node head;
    private Node tail;

    private int size;
    private int minAge;
    private int maxAge;

    public LinkedCyclicAutoOrderList() {
        clear();
    }

    private static class Node {
        private Car value;
        private Node next;

        public Node(Car value) {
            setValue(value);
        }

        public Car getValue() {
            return value;
        }

        public void setValue(Car value) {
            this.value = value;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }
    }

    @Override
    public void add(Object value) {
        Car carToAdd = (Car) value;
        int ageOfCar = carToAdd.getAge();

        Node nodeToAdd = new Node(carToAdd);

        if (head == null) {
            initialElement(ageOfCar, nodeToAdd);
        } else {
            addCarToListByAge(ageOfCar, nodeToAdd);
        }

        size++;
    }

    private void initialElement(int age, Node nodeToAdd) {
        head = nodeToAdd;
        tail = nodeToAdd;
        head.setNext(tail);
        tail.setNext(head);
        minAge = age;
        maxAge = age;
    }

    private void addCarToListByAge(int age, Node nodeToAdd) {
        if (age >= maxAge) {
            maxAge = age;
            addAsLast(nodeToAdd);
        } else if (age <= minAge) {
            minAge = age;
            addAsFirst(nodeToAdd);
        } else {
            addAmongOthers(age, nodeToAdd);
        }
    }

    private void addAsFirst(Node nodeToAdd) {
        nodeToAdd.setNext(head);
        head = nodeToAdd;
        tail.setNext(nodeToAdd);
    }

    private void addAsLast(Node nodeToAdd) {
        tail.setNext(nodeToAdd);
        tail = nodeToAdd;
        tail.setNext(head);
    }

    private void addAmongOthers(int age, Node nodeToAdd) {
        Node previous = head;
        Node current = head;

        while (current.getValue().getAge() < age) {
            previous = current;
            current = current.getNext();
        }

        nodeToAdd.setNext(current);
        previous.setNext(nodeToAdd);
    }

    private Node findNode(int index) throws IndexOutOfBoundsException {
        if (index >= size || index < 0) {
            throw new IndexOutOfBoundsException();
        } else {
            Node currentNode = head;

            for (int current = 0; current < index; current++) {
                currentNode = currentNode.getNext();
            }

            return currentNode;
        }
    }

    @Override
    public void delete(Object value) {
        boolean found = false;

        if (size > 0) {
            if (head.getValue().equals(value)) {
                deleteHead();
                found = true;
            } else if (tail.getValue().equals(value)) {
                deleteTail();
                found = true;
            } else {
                found = deleteNodeAmongOthers(value);
            }
        }

        if (found) {
            System.out.println("Element zostal usutniety");
            size--;

            if (head != null) {
                minAge = head.getValue().getAge();
                maxAge = head.getValue().getAge();
            }

        } else {
            System.out.println("Element, ktory chcesz usunac nie zostal znaleziony");
        }
    }

    private boolean deleteNodeAmongOthers(Object value) {
        boolean found = false;

        Node previous = head;
        Node current = head.getNext();
        int index = 0;

        while (!found && index < size) {
            if (current.getValue().equals(value)) {
                found = true;
                previous.setNext(current.getNext());
            } else {
                previous = current;
                current = current.getNext();
                index++;
            }
        }

        return found;
    }

    private void deleteTail() {
        Node previous = head;
        Node current = head.getNext();

        while (current != tail) {
            previous = current;
            current = current.getNext();
        }
        previous.setNext(head);
    }

    private void deleteHead() {
        if (head == tail) {
            head = tail = null;
        } else {
            head = head.getNext();
            tail.setNext(head);
        }
    }

    @Override
    public Object get(int index) throws IndexOutOfBoundsException {
        try {
            return findNode(index).getValue();
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Nie znaleziono takiego elementu");
        }

        return new Car("none", 0);
    }

    @Override
    public void set(int index, Object value) throws IndexOutOfBoundsException {
        try {
            delete(findNode(index).getValue());
            add(value);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Nie znaleziono takiego elementu");
        }
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void clear() {
        head = null;
        tail = null;
        minAge = 0;
        maxAge = 0;
        size = 0;
    }
}
