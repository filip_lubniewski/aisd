import Utils.Car;
import Utils.List;

/**
 * Created by macbookpro on 06.03.2016.
 */
public class LinkedAcyclicSentinelList implements List {

    private final Node headSentinel = new Node(null);
    private final Node tailSentinel = new Node(null);

    private int size;

    private static class Node {
        private Car value;
        private Node next;

        public Node(Car value) {
            setValue(value);
        }

        public Car getValue() {
            return value;
        }

        public void setValue(Car value) {
            this.value = value;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }
    }

    public LinkedAcyclicSentinelList() {
        clear();
    }

    private Node findNode(int index) throws IndexOutOfBoundsException {
        if (index >= size || index < 0) {
            throw new IndexOutOfBoundsException();
        } else {
            Node currentNode = headSentinel.getNext();

            for (int current = 0; current < index; current++) {
                currentNode = currentNode.getNext();
            }

            return currentNode;
        }
    }

    private Node getLastNode() {
        if (size == 0) {
            return headSentinel;
        } else {
            return findNode(size - 1);
        }
    }

    @Override
    public void add(Object value) {
        Node newNodeToAdd = new Node((Car) value);

        newNodeToAdd.setNext(tailSentinel);
        getLastNode().setNext(newNodeToAdd);

        size++;
    }

    @Override
    public void delete(Object value) {
        boolean found = false;

        Node previousNode = headSentinel;
        Node currentNode = previousNode.getNext();

        while (currentNode != tailSentinel && !found) {
            if (currentNode.getValue().equals(value)) {
                found = true;
                previousNode.setNext(currentNode.getNext());
            } else {
                previousNode = currentNode;
                currentNode = currentNode.getNext();
            }
        }

        if (!found) {
            System.out.println("Nie znaleziono takiego elementu do usunięcia");
        } else {
            System.out.println("Usunięto wskazany element");
            size--;
        }
    }

    @Override
    public Object get(int index) {
        try {
            return findNode(index).getValue();
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Nie znaleziono takiego elementu");
        }

        return new Car("none", 0);
    }

    @Override
    public void set(int index, Object value) {
        try {
            findNode(index).setValue((Car) value);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Nie znaleziono takiego elementu");
        }
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void clear() {
        headSentinel.setNext(tailSentinel);
        tailSentinel.setNext(null);

        size = 0;
    }
}
