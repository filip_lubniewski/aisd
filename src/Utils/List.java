package Utils;

/**
 * Created by macbookpro on 06.03.2016.
 */
public interface List {
    void add(Object value);

    void delete(Object value);

    Object get(int index) throws IndexOutOfBoundsException;

    void set(int index, Object value) throws IndexOutOfBoundsException;

    boolean isEmpty();

    int size();

    void clear();
}
