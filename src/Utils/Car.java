package Utils;

/**
 * Created by macbookpro on 06.03.2016.
 */
public class Car {
    private String brand;
    private int age;

    public Car(String brand, int age) {
        this.brand = brand;
        this.age = age;
    }

    @Override
    public boolean equals(Object obj) {
        Car car = (Car) obj;

        return car.brand.equals(this.brand) && car.age == this.age;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
