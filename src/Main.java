import Utils.Car;

/**
 * Created by macbookpro on 05.03.2016.
 */
public class Main {

    public static void main(String[] args) {

        System.out.println("\nTEST Linked Acyclic Sentinel List: ");


        LinkedAcyclicSentinelList list = new LinkedAcyclicSentinelList();

        list.delete(new Car("abc", 0));
        list.add(new Car("FORD", 8));
        list.delete(new Car("abc", 0));

        list.add(new Car("PORSCHE", 12));
        list.add(new Car("HONDA", 3));

        Car honda = (Car) list.get(2);
        System.out.println(honda.getBrand() + "\n");

        list.delete(honda);

        for(int i = 0; i < list.size(); i++)
        {
            System.out.println(((Car) list.get(i)).getBrand());
        }

        System.out.println("\nTEST LinkedCyclicAutoOrderList: ");

        Car car1 = new Car("1", 15);
        Car car2 = new Car("2", 9);
        Car car3 = new Car("3", 12);
        Car car4 = new Car("1", 2);
        Car car5 = new Car("2", 4);

        LinkedCyclicAutoOrderList listaPosegregowana = new LinkedCyclicAutoOrderList();

        listaPosegregowana.add(car1);
        listaPosegregowana.add(car2);
        listaPosegregowana.add(car3);
        listaPosegregowana.add(car4);
        //listaPosegregowana.add(car5);


        for (int i = 0; i < listaPosegregowana.size(); i++) {
            System.out.println(((Car) listaPosegregowana.get(i)).getAge());
        }

        listaPosegregowana.delete(car5);

        System.out.println("Po usunieciu: ");

        for (int i = 0; i < listaPosegregowana.size(); i++) {
            System.out.println(((Car) listaPosegregowana.get(i)).getAge());
        }

        ArrayAutoOrderList listaTablicowa = new ArrayAutoOrderList();

        listaTablicowa.add(car1);
        listaTablicowa.add(car2);
        listaTablicowa.add(car3);
        listaTablicowa.add(car4);
        listaTablicowa.add(car5);

        listaTablicowa.delete(car4);

    }
}
